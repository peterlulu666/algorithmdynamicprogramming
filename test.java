import java.util.Arrays;

public class test {
    public static void main(String[] args) {
        int start = 0;
        int end = 3;
        int totalFuel = 10;
        int numberOfPlanet = 4;

        double infinity = Double.POSITIVE_INFINITY;
        int[][] dp = new int[numberOfPlanet][totalFuel + 1];
        for (int j = 0; j < numberOfPlanet; j++) {
            for (int i = 0; i < totalFuel + 1; i++) {
                dp[j][i] = (int) infinity;
            }
        }

        for (int i = 0; i < totalFuel + 1; i++) {
            dp[numberOfPlanet - 1][i] = 0;
        }

        System.out.println(Arrays.deepToString(dp));

    }
}
