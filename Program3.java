import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Program3 {

    DamageCalculator calculator;
    PlanetPathScenario planetScenario;

    public Program3() {
        this.calculator = null;
        this.planetScenario = null;
    }

    /*
     * This method is used in lieu of a required constructor signature to initialize
     * your Program3. After calling a default (no-parameter) constructor, we
     * will use this method to initialize your Program3 for Part 2.
     */
    public void initialize(PlanetPathScenario ps) {
        this.planetScenario = ps;
    }

    /*
     * This method is used in lieu of a required constructor signature to initialize
     * your Program3. After calling a default (no-parameter) constructor, we
     * will use this method to initialize your Program3 for Part 1.
     */
    public void initialize(DamageCalculator dc) {
        this.calculator = dc;
    }


    /*
     * This method returns an integer that is the minimum amount of time necessary to travel
     * from the start planet to the end planet in the PlanetPathScenario given the total
     * amout of fuel that Thanos has. If a path is not possible given the amount of fuel, return -1.
     */
    //TODO: Complete this method

    // 4.in
    /*
    * [
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]
      ]

      [
      [inf, inf, inf, inf, inf, inf, 10, 10, 10, 10, 8],
      [inf, inf, inf, 5,   5,   5,   5,  5,  5,  5,  5],
      [inf, inf, inf, inf, inf, inf, 4,  4,  4,  4,  4],
      [0,   0,   0,   0,   0,   0,   0,  0,  0,  0,  0]
      ]
     * */

    // 8.in
    /*
    * [
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]
      ]

      [
      [inf, inf, inf, inf, inf, inf, inf, 15, 15, 14, 14],
      [inf, inf, inf, inf, inf, 11,  11,  10, 9,  9,  9],
      [inf, inf, inf, inf, inf, inf, 7,   7,  7,  7,  7],
      [inf, inf, inf, inf, inf, 6,   6,   6,  6,  6,  6],
      [inf, inf, inf, 5,   5,   5,   5,   5,  5,  5,  5],
      [inf, inf, inf, inf, 5,   5,   5,   5,  5,  5,  5],
      [inf, inf, inf, 2,   2,   2,   2,   2,  2,  2,  2],
      [0,   0,   0,   0,   0,   0,   0,   0,  0,  0,  0]
      ]
    * */
    public int computeMinimumTime() {
        int start = planetScenario.getStartPlanet();
        int end = planetScenario.getEndPlanet();
        int totalFuel = planetScenario.getTotalFuel();

        // it does not make any sense if the fuel is 0 or if it starts and ends at the same planet
        // if the the fuel is 0, it will not be able to go anywhere
        // if the start and end planets are the same, we would not run the algorithm to find the minimum time
        if (totalFuel == 0 || start == end) {
            return -1;
        }

        // build up the table

        // initialize every row and every column to be infinity
        double infinity = Double.POSITIVE_INFINITY;
        int[][] dp = new int[end + 1][totalFuel + 1];
        for (int row = 0; row <= end; row++) {
            for (int column = 0; column <= totalFuel; column++) {
                dp[row][column] = (int) infinity;
            }
        }

        // initialize the last row to be 0
        for (int column = 0; column <= totalFuel; column++) {
            dp[end][column] = 0;
        }

        // build up the graph
        // store the graph in the HashMap
        // the key is the vertex
        // the value is the adjacent vertex and arrayList contains fuel and time

        // store the vertex into the HashMap
        HashMap<Integer, HashMap<Integer, ArrayList<Integer>>> graph = new HashMap<Integer, HashMap<Integer, ArrayList<Integer>>>();
        for (int planet = start; planet <= end; planet++) {
            graph.put(planet, new HashMap<Integer, ArrayList<Integer>>());
        }

        // store the adjacent vertex and the edge weight into the HashMap
//        for (int planet = start; planet <= end; planet++) {
//            int numberOfAdjacentPlanet = planetScenario.getFlightsFromPlanet(planet).length;
//            while (numberOfAdjacentPlanet > 0) {
//                ArrayList<Integer> fuelTime = new ArrayList<Integer>();
//                fuelTime.add(0, planetScenario.getFlightsFromPlanet(planet)[numberOfAdjacentPlanet - 1].getFuel());
//                fuelTime.add(1, planetScenario.getFlightsFromPlanet(planet)[numberOfAdjacentPlanet - 1].getTime());
//                graph.get(planet).put(planetScenario.getFlightsFromPlanet(planet)[numberOfAdjacentPlanet - 1].getDestination(), fuelTime);
//                numberOfAdjacentPlanet--;
//            }
//        }

        for (int planet = start; planet <= end; planet++) {
            int numberOfAdjacentPlanet = planetScenario.getAllFlights()[planet].length;
            while (numberOfAdjacentPlanet > 0) {
                ArrayList<Integer> fuelTime = new ArrayList<Integer>();
                fuelTime.add(0, planetScenario.getAllFlights()[planet][numberOfAdjacentPlanet - 1].getFuel());
                fuelTime.add(1, planetScenario.getAllFlights()[planet][numberOfAdjacentPlanet - 1].getTime());
                graph.get(planet).put(planetScenario.getAllFlights()[planet][numberOfAdjacentPlanet - 1].getDestination(), fuelTime);
                numberOfAdjacentPlanet--;
            }
        }

//        for (int fuel = 0; fuel <= totalFuel; fuel++) {
//            for (int planet = start; planet <= end; planet++) {
//                SpaceFlight[] spaceFlight = planetScenario.getAllFlights()[planet];
//                for (int i = 0; i < spaceFlight.length; i++) {
//                    if (spaceFlight[i].getFuel() <= fuel) {
//                        if (dp[spaceFlight[i].getDestination()][fuel - spaceFlight[i].getFuel()] < (int) infinity) {
//                            dp[planet][fuel] = Math.min(spaceFlight[i].getTime() + dp[spaceFlight[i].getDestination()][fuel - spaceFlight[i].getFuel()], dp[planet][fuel]);
//                        }
//                    }
//                }
//            }
//        }

        for (int fuel = 0; fuel <= totalFuel; fuel++) {
            for (int planet = start; planet <= end; planet++) {
                HashMap<Integer, ArrayList<Integer>> adjacentPlanets = graph.get(planet);
                for (Map.Entry<Integer, ArrayList<Integer>> entry : adjacentPlanets.entrySet()) {
                    // the adjacent planet
                    int adjacentPlanet = entry.getKey();
                    // the fuel it will take to travel to the adjacent planet
                    int adjacentFuel = entry.getValue().get(0);
                    // the time it will take to travel to the adjacent planet
                    int adjacentTime = entry.getValue().get(1);
                    if (adjacentFuel <= fuel) {
                        if (dp[adjacentPlanet][fuel - adjacentFuel] < (int) infinity) {
                            dp[planet][fuel] = Math.min(dp[planet][fuel], adjacentTime + dp[adjacentPlanet][fuel - adjacentFuel]);
                        }
                    }
                }
            }
        }

        return dp[start][totalFuel];

    }

    /*
     * This method returns an integer that is the maximum possible damage that can be dealt
     * given a certain amount of time.
     */
    //TODO: Complete this function
    public int computeDamage() {

        int totalTime = calculator.getTotalTime();
        int numAttacks = calculator.getNumAttacks();

        // build up the table
        int[][] dp = new int[numAttacks + 1][totalTime + 1];
        for (int row = 0; row <= numAttacks; row++) {
            for (int column = 0; column <= totalTime; column++) {
                dp[row][column] = 0;
            }
        }

        for (int attack = 0; attack < numAttacks; attack++) {
            for (int time = 1; time <= totalTime; time++) {
                for (int attackTime = 0; attackTime <= time; attackTime++) {
                    dp[attack + 1][time] = Math.max(dp[attack + 1][time], calculator.calculateDamage(attack, attackTime) + dp[attack][time - attackTime]);
                }
            }
        }

        return dp[numAttacks][totalTime];

    }
}


